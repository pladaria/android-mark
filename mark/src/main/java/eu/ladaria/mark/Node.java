package eu.ladaria.mark;

import java.util.LinkedList;

public class Node {

    final String type;
    final Node parent;
    boolean closed = false;
    String text = "";
    final LinkedList<Node> children = new LinkedList();

    private Node(String type, Node parent) {
        this.type = type;
        this.parent = parent;
    }

    public static Node create(String type, Node parent) {
        return new Node(type, parent);
    }
}
