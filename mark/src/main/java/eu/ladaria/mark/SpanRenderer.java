package eu.ladaria.mark;

import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.StrikethroughSpan;
import android.text.style.StyleSpan;
import android.text.style.TypefaceSpan;

public class SpanRenderer {

    private SpanRenderer() {}

    public static SpanRenderer create() {
        return new SpanRenderer();
    }

    private CharSequence join(Node node, boolean raw) {
        CharSequence res = "";
        for (Node n: node.children) {
            res = TextUtils.concat(res, render(n, raw));
        }
        return res;
    }

    private CharSequence style(String t, CharSequence cs) {
        Spannable s;
        switch (t) {
            case "`":
                s = new SpannableString(cs);
                s.setSpan(new TypefaceSpan("monospace"), 0, cs.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                return s;
            case "*":
                s = new SpannableString(cs);
                s.setSpan(new StyleSpan(Typeface.BOLD), 0, cs.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                return s;
            case "_":
                s = new SpannableString(cs);
                s.setSpan(new StyleSpan(Typeface.ITALIC), 0, cs.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                return s;
            case "~":
                s = new SpannableString(cs);
                s.setSpan(new StrikethroughSpan(), 0, cs.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                return s;
            default:
                return cs;
        }
    }

    public CharSequence render(Node ast) {
        return render(ast, false);
    }

    private CharSequence render(Node ast, boolean raw) {
        String t = ast.type;
        if (t.length() > 0 && ast.closed) {
            if ("`".equals(t)) {
                return style(t, Spannable.Factory.getInstance().newSpannable(join(ast, true)));
            }
            return raw ? TextUtils.concat(t, join(ast, true), t) : style(t, join(ast, false));
        }
        return TextUtils.concat(ast.text, t, join(ast, raw));
    }

}
