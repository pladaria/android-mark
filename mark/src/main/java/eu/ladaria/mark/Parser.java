package eu.ladaria.mark;

import java.util.Stack;

public class Parser {

    private static String MARKS = "_~*`";
    private static String SPACE = "\n\r\t ";
    private static String PUNCTUATION = ",.;:?!()[]{}/-\"'";

    private static boolean opens(String breaks, Character prev, Character next) {
        return (prev == null || breaks.indexOf(prev) >= 0) && (next != null && SPACE.indexOf(next) < 0);
    }

    private static boolean closes(String breaks, Character next) {
        return next == null || breaks.indexOf(next) >= 0;
    }

    public static Node parse(final String str) {
        return parse(str, MARKS);
    }

    public static Node parse(final String str, String marks) {
        String breaks = PUNCTUATION + MARKS + SPACE;
        Node ast = Node.create("", null);
        Node node = Node.create("", ast);
        ast.children.add(node);

        Character c, n, p;
        Node tmp, par;
        Stack<Character> stack = new Stack<>();
        for (int i = 0, len = str.length(); i < len; i++) {
            p = (i > 0) ? str.charAt(i - 1) : null;
            c = str.charAt(i);
            n = (i < len - 1) ? str.charAt(i + 1) : null;
            if (marks.indexOf(c) >= 0) {
                if (closes(breaks, n) && stack.contains(c)) {
                    while (stack.size() > 0) {
                        node = node.parent;
                        if (stack.pop() == c) {
                            node.closed = true;
                            par = node.parent;
                            node = Node.create("", par);
                            par.children.add(node);
                            break;
                        }
                    }
                    continue;
                }
                if (opens(breaks, p, n) && !stack.contains(c)) {
                    stack.push(c);
                    par = node.parent;
                    tmp = Node.create(String.valueOf(c), par);
                    par.children.add(tmp);
                    node = Node.create("", tmp);
                    tmp.children.add(node);
                    continue;
                }
            }
            node.text += c;
        }
        return ast;
    }

}
