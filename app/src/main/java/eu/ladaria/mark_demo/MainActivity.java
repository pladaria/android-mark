package eu.ladaria.mark_demo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import eu.ladaria.mark.Node;
import eu.ladaria.mark.Parser;
import eu.ladaria.mark.SpanRenderer;

public class MainActivity extends AppCompatActivity {

    EditText resultText, inputText;
    SpanRenderer renderer = SpanRenderer.create();
    long renderTime, parseTime;

    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
        @Override
        public void afterTextChanged(Editable editable) {}
        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            mark();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        resultText = (EditText)findViewById(R.id.resultText);
        inputText = (EditText)findViewById(R.id.inputText);
        inputText.addTextChangedListener(textWatcher);
        mark();
    }

    private final int BENCHMARK_TIMES = 100;

    private void mark() {
        Node ast = Node.create("", null);
        int i;

        String str = String.valueOf(inputText.getText());
        CharSequence result = "";

        // benchmark parse
        parseTime = System.nanoTime();
        for (i = 0; i < BENCHMARK_TIMES; i++) {
            ast = Parser.parse(str);
        }
        parseTime = (System.nanoTime() - parseTime) / BENCHMARK_TIMES;

        // benchmark render
        renderTime = System.nanoTime();
        for (i = 0; i < BENCHMARK_TIMES; i++) {
            result = renderer.render(ast);
        }
        renderTime = (System.nanoTime() - renderTime) / BENCHMARK_TIMES;

        // show results
        resultText.setText(result);
        setTitle("Parse: " + String.format("%.3f", parseTime / 1e6) + "ms - Render: " + String.format("%.3f", renderTime / 1e6) + "ms");
    }
}
